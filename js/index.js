$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({ interval: 2000 });
    $('#contacto').on('show.bs.modal',function (e) {
      console.log('mostarado modal');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled',true);
      $('#contactoBtn2').removeClass('btn-outline-success');
      $('#contactoBtn2').addClass('btn-primary');
      $('#contactoBtn2').prop('disabled',true);
       
    });
    
    $('#contacto').on('shown.bs.modal',function (e) {
      console.log('modal se mostro');
    });
    $('#contacto').on('hide.bs.modal',function (e) {
      console.log('ocultando modal');
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled',false);
      $('#contactoBtn2').removeClass('btn-primary');
      $('#contactoBtn2').addClass('btn-outline-success');
      $('#contactoBtn2').prop('disabled',false);
    });
    $('#contacto').on('hidden.bs.modal',function (e) {
      console.log('modal oculto');
    });

  });